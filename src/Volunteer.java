public class Volunteer extends StaffMember {
    public Volunteer(int id, String name, String address) {
        super(id, name, address);
    }

    @Override
    public String toString() {
        return "" +
                "ID: " + id +"\n"+
                "Name: " + name + "\n"+
                "Address: " + address + "\n"+
                "--------------------------------";
    }
}
