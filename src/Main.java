import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class Main {
    int option;
    public void menu(int opiont){
        this.option = opiont;
        System.out.println("===============>Menu<==============");
        System.out.println("1. Add Employee");
        System.out.println("2. Update Employee");
        System.out.println("3. Remove Employee");
        System.out.println("4. Exit");
        System.out.println("-----------------------------------");
        System.out.print("=> Choose Option(1-4) : ");
    }
    public static void main(String[] args) throws IOException {
        Main main = new Main();
        int op = 0;
        int option = 0;
        int id,hoursWorked,rate;
        String name,address;
        double salary,bonus;
        Scanner obj = new Scanner(System.in);

        ArrayList<StaffMember> staffMembers = new ArrayList<StaffMember>();
        StaffMember staffMember1 = new Volunteer(1,"លោកបង១៦៨","Kratie");
        StaffMember staffMember2 = new SalariedEmployee(2,"ក្រមុំស្រុកស្អាង","Kandal",290.0,30);
        StaffMember staffMember3 = new HourlyEmployee(3,"Kimchang","PP",200,90);

        staffMembers.add(staffMember1);
        staffMembers.add(staffMember2);
        staffMembers.add(staffMember3);
        for (StaffMember staffMember : staffMembers){
            System.out.println(staffMember.toString());
        }

        main.menu(option);
        option = obj.nextInt();
        while (option <=3)
        switch (option){
            case 1:
                System.out.println("----------------------------");
                System.out.println("1. volunteer ");
                System.out.println("2. Hourly Employee ");
                System.out.println("3. Salaried Employee");
                System.out.println("4. Back");
                System.out.println("----------------------------");
                System.out.println("Choose Option(1-4):");
                op = obj.nextInt();

                    switch (op) {
                        case 1: {
                            System.out.println("================ADD VOLUNTEER================");
                            System.out.print("Input ID: ");
                            id = obj.nextInt();
                            System.out.print("Input NAME: ");
                            name = obj.next();
                            System.out.print("Input ADDRESS: ");
                            address = obj.next();
                            Volunteer vol1 = new Volunteer(id, name, address);
                            staffMembers.add(vol1);

                            for (StaffMember v1 : staffMembers) {
                                System.out.println(v1.toString());
                            }
                            System.in.read();
                        }
                        break;
                        case 2: {
                            System.out.println("================ADD HOURLY EMPLOYEE================");
                            System.out.print("Input ID: ");
                            id = obj.nextInt();
                            System.out.print("Input NAME: ");
                            name = obj.next();
                            System.out.print("Input ADDRESS: ");
                            address = obj.next();
                            System.out.print("Input HOURS WORKED: ");
                            hoursWorked = obj.nextInt();
                            System.out.print("Input RATE: ");
                            rate = obj.nextInt();

                            HourlyEmployee he1 = new HourlyEmployee(id, name, address, hoursWorked, rate);
                            staffMembers.add(he1);
                            for (StaffMember he : staffMembers) {
                                System.out.println(he.toString());
                            }
                            System.in.read();
                        }
                        break;
                        case 3: {
                            System.out.println("================ADD SALARIED EMPLOYEE================");
                            System.out.print("Input ID: ");
                            id = obj.nextInt();
                            System.out.print("Input NAME: ");
                            name = obj.next();
                            System.out.print("Input ADDRESS: ");
                            address = obj.next();
                            System.out.print("Input SALARY: ");
                            salary = obj.nextDouble();
                            System.out.print("Input BONUS: ");
                            bonus = obj.nextDouble();

                            SalariedEmployee salariedEmployee = new SalariedEmployee(id, name, address, salary, bonus);
                            staffMembers.add(salariedEmployee);
                            for (StaffMember se1 : staffMembers) {
                                System.out.println(se1.toString());
                            }
                            System.in.read();

                        }
                        break;
                    }
                    if(option>3 || option<1){
                        do{
                            op = obj.nextInt();
                        }while (option>3);
                    }
            }
        }
    }


