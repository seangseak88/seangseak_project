public class HourlyEmployee extends StaffMember {
    private int hoursWorked;
    private double rate;
    public HourlyEmployee(int id, String name, String address,int hoursWorked,double rate) {
        super(id, name, address);
       this.hoursWorked = hoursWorked;
       this.rate = rate;

        pay = (Double) rate * hoursWorked;

    }



    @Override
    public String toString() {
        return "" +
                "ID " + id +"\n"+
                "Name " + name +"\n"+
                "Address " + address +"\n"+
                "Hours Worked "+hoursWorked+"\n"+
                "Rate "+rate+"\n"+
                "Payment " + pay+"\n"+
                "--------------------------------";
    }
}
