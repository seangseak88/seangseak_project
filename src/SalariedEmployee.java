public class SalariedEmployee extends StaffMember {
    private double salary,bonus;
    public SalariedEmployee(int id, String name, String address,double salary,double bonus) {
        super(id, name, address);
        this.salary = salary;
        this.bonus = bonus;

        pay = salary + bonus;
    }

    @Override
    public String toString() {
        return "" +
                "ID: " + id +"\n"+
                "Name: " + name + "\n"+
                "Address: " + address + "\n"+
                "Salary: " + salary +"\n"+
                "Bonus: " + bonus +"\n"+
                "Payment: " + pay+ "\n"+
                "--------------------------------";

    }
}
